# Animações CSS

### Hover
A propriedade hover é colocada para que quando o cursor esteja em cima do elemento com essa propriedade, ele tenha um outro comportamento.

No caso abaixo, ele assumira as outro BGC e outro Color quando o cursor estiver em cima dele.
```
.button-store:hover{
background-color:#FFF;
color: #e24647;
```

### Transition
Usamos o transition para que o efeito a do :hover seja suavizado. O Transition é adicionado ao elemento pai.

```
.button-store{
    display: inline-block;
    .
    .
    .
    .
    
    transition: background-color 1s linear,
    color 2s linear;
    
 }
```
### Active
Evento de click.
Podemos usar junto com o transform:scale(MULTIPLICADOR) Quando o usuário clicar no botao ou elemento ele pode diminuir ou aumentar.
O *scale* é um multiplicador. Se colocarmos 2 ele faz o tamanho do elemento x2. 
```
.button-store:active{
transform: scale(0.9)
}
```