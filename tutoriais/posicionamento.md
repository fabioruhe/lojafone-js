# Posicionamento CSS

1. **Float**
    - Aplicação: Todos os elementos;
    - Sintaxe:
        - float: left
        - float: right
    
    Esta propriedade faz com que o elemento no qual ela for aplicada saia do contexto normal e flutue à direita ou a esquerda. Ao utiliza-la, os elementos em linha irao se posicionar ao seu redor.

2. **Overflow**
    
    Utilizamos no elemento pai do float para que o contexto seja calculado corretamente.
    *ajustar*

3. **Margin**

    Utilizada para separar elemento na tela. Ou, dar um respiro externo entre elementos.
    - Sintaxe:
        -/* padding/margin: top/right/bottom/left 4 Valores */

        /* padding/margin: top/left right/bottom/left 3 Valores */

        /* padding/margin: top bottom/left right 2 Valores */

4. **Padding**

    o espaço ao redor do conteúdo (ex.: ao redor do texto de um parágrafo). A sintaxe é identica ao do margin.


## Display

1. 

   ```
   Inline
   ```
- Permite elementos na mesma linha;
- Não permite definirmos Largura e Altura;
- Comportamento de uma palavra (conteudo).

2.

  ```
    Block
  ```
- Não permite elementos na mesma linha;
- Permite definirmos Largura e Altura;

3. 

```
Inline-block
```
- Permite elementos na mesma linha;
- Permite definirmos Largura e Altura;
- Comportamento de uma palavra(conteudo).



### Alinhamento Display:Inline/Inline-block

#### Caso não seja setado eles serão alinhados pela base da linha (base line).

![Base Line](\base-line.png)

*para corrigir, basta usar o vertical align:top o elemento precisar ser inline-block e estar na mesma linha*



## Position

**1. Static**

**2. Absolute**

-Cria um novo contexto;

-Quem define o tamanho do elemento é o conteudo;

-Movemos os elementos em relação a pagina (top,right,bottom,left)

   1. Top  (eixo x)
   2. Right(eixo y)
   3. Bottom(eixo x)
   4. Left(eixo y)

   Usa-se sempre uma para mexer no eixo X e outra no Y. Não faz sentido usar mais de uma para cada eixo.
    

  Quando usamos as propriedades a cima, temos que pensar que sempre será levado em consideração para alinhar, a borda do elemento.

   **Quando utlizamos valores com % nos eixos, a referencia será semrpe o elemento pai.**
    
   Como com as propriedades (top,right...) alinham a borda do elemento, para conseguirmos centrarlizar, utlizamos a propriedade Transform(translate):

   ```
   transform: translateX (-50%) - Valores negativos = Esquerda / Positivos = Direita
   transform: translateY (60%) - Valores negativos = Desce / Positivos = Sobe
   transform: translate (-50% , 60%) - Modo resumido (X,Y)
```
**Transform(translate) usa como referencia o propio elemento _e não o pai como o top/right_**

  


**3. Relative**

**4. Fixed**






# Alinhamento de texto

1. Horizontal

```
text-align: left/right/center;
```

2. Vertical (para uma unica linha, tipo botão)
```
line-height: sempre do tamanho da altura do elemento(height).
```