# Boas Práticas - CSS

1. **Nunca usar seletor de tag**
    
     Com seletores de tag não conseguimos flexibilidade para conseguir manipular os elementos e o CSS de forma livre. 
      

2. **Evitar usar ID`s**
  
    Utilizar ID's para partes únicas da estrutura de seu layout, como: cabeçalho, rodapé, topo, menu. Porém, se desejar utilizar o ID #cabecalho por exemplo repetidas vezes na página não será possível. **Novamente, utilizando class temos mais poder e flexibilidade para mexer no código. ID é a identidade de uma tag. É único.**

3. **Componentes "grandes"**
    Sempre utilizar 2 nomes para compomentes grandes (header,footer,container), por exemplo:
    ```
    .header-store{
 
    background-color: #e24647;
    color: #fff;
    overflow: hidden;
    ...
    ...
    ```
    Para os componentes dentro do header(exemplo) utilizamos nomes simples.

 4. **Uma classe dentro da outra**
    Quando precisamos que um elemento tenha todos os comportamento da classe porém com um algo a mais conseguimos colocar esse algo a mais em uma classe separada no CSS. No HTML separamos os nomes pode com 'espaço'.

    **Quando não temos muita experiencia e estamos fazendo as classes, é bom sempre refazer o elemento por completo e depois refatorar para vermos o que um tem de igual ao outro e mandar o codigo mais limpo.**

    HTML:
    ```
    <a class="action" href="#">Produtos</a>
    <a class="action" href="#">Serviços</a>
    <a class="action last" href="#">Carrinho (10)</a>
    ```
    *O link "Carrinho(10)" precisa ter um comportamento diferente por ser o ultimo e estar mais proximo a margem.

    CSS: action:
      ```
    .action{
    color:inherit;
    text-decoration: none;
    margin-right:60px;}
    ```
    CSS: action last:
    ```
    .last{
    margin: 0;}
    ```

# Nome de classes

- Componentes grandes utilizamos 2 nomes:

        -header-store;

        -container-store;

- Variações de elementos, utilizaremos sempre um - :

    -Elemento principal:

    ```
    .button-store
    ```
    -Variação:

     ```
    .-second
    ```
    HTML:'
    ```
    <a class="button-store -second" href="comprar.html">Adicionar ao Carrinho</a>
    <a class="button-store" href="comprar.html">Comprar</a>
    ```
### Na variaçao é sempre bom indicar no CSS que aquela variação vale apenas para um elemento especifico. No caso a cima se aplicarmos em outro elemento que nao seja a variação do botao, o elemento ira assumir todo o comportamento -second.
Para fixarmos a variação no botão, deixamos o CSS da seguinte forma:

```
.button-store.-second{
    background-color: transparent;
    border: 2px solid #FFF;
    width: 218px;
    line-height: 56px;
 }
```
 Onde button-store é o principal e -second é a variação.
 Para fixar colocamos no css classe_principal.variação.
#### SELETOR (E(AND))!

# Seletores Filhos

- São considerados seletores filhos os seletores que estiverem dentro de outra tag.
    ```
    <nav>PAI
        <div>filho
        <a>filho
        <a>filho
        </div>filho
    </nav>
    ```

Quando querermos utilizar uma classe com o mesmo nome no CSS, por ex TITLE. No CCS precisamos deixar claro que é somente para filhos do mesmo elemento.
Temos o *header-store.css* que dentro tem a classe pai (header) .header-store.
Dentro do header-store temos um title referente ao logo, porém, na descrição do produto tambem teremos um title. Se simplesmente aplicarmos o title ele vai ter o mesmo comportamento da logo. Para separar, dentro do css header-store deixarmos os elementos da seguinte forma:

Elemento Pai:
```
.header-store{
    
    
    color: #fff;
    overflow: hidden;
    /* margin: 0 115px; */
```

Filhos:
```
.header-store .title{
    
    
    font-size: 40px;
    font-family: Arial, 'Roboto', sans-serif;
    float: left;
    

    
}
.header-store .navigation{
    font-size: 21px;
    font-family: 'Segoe UI', Tahoma, Geneva, Verdana, sans-serif;
     float: right;
    
}
```
Desta forma, cada um será aplicado somente dentro do header-store.
Para classes modificadoras o CSS fica da seguinte forma:

```
.header-store .action.-last{
    margin: 0;
}
```

# Filhos Diretos

```
    <div class="container-store">
        <header class="header-store">
            <h1 class="title">LOGO</h1>
            <nav class="navigation">
                <a class="action" href="#">Produtos</a>
                <a class="action" href="#">Serviços</a>
                <a class="action -last" href="#">Carrinho (10)</a>
            </nav>

        </header>
```

# Descrevendo Coisas no HTML

HTML:
```
DL - Description List
DT - Description Title
DD - Define Description
- - - 
<dl>
<dt>
<dd>
```
    

