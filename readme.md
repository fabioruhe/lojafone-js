# Loja Fone - JS

### Selecionando o elemento no HTML via JS (Pegar Elemento)

const $nomeDaVariavel = document.quesrySelector("Classe do HTML")

```
document = Chama o HTML;
querySelector = Busca o seletor(classe);
```
Para podermos trabalhar com o elemento, adicionamos ele em uma variavel.
Nesse caso na **const**.

Utilizamos sempre **const** quando são valores que não serão alterados. Para valores que são alterados/incrementados utilizamos **let**.


## EventListener
### Responsavel por "ouvir" um evento que irá acontecer no browser.

$nomeDaVariavel = document.addEventListener("Click", handleClick)

**Click** é o evento que será esperado pelo codigo.

**handleClick** é a função que irá liberar os comandos quando o usuário clicar no elemento selecionado.

```
function handleClick(){
    console.log("Oi");
    }
```

## Incrementando Valores

```
let valorInicial = 0;

function handleClick(){
    const $carrinho = document.querySelector(".-last");
    $carrinho.textContent = ++valorInicial;
}
```
No caso a cima, valorInicial está recebendo +1 e incrementando a variavel, logo ela passa a valer 2 a cada click no botão.
Caso o + estivesse depois, ele primeiro atribuiria o valor e no proximo click somaria.

### TemplateString

Utilizado para contanar String+Variavel.
Ao invés de utilizar "String" + Variavel + "String - O que pode dificultar muito a leitura, logo, podemos utlizar o template string:

Template strings são envolvidas por acentos graves (` `) em vez de aspas simples ou duplas.
Estes são indicados por um cifrão seguido de chaves (${expression})

```
 $carrinho.textContent = `Carrinho  (${++valorInicial})`;
 [resultado] Carrinho(++valorInicial) - Sempre incrementando quando clica no botao.
```
[`texto string ${expression} texto string`']

### Modificando Classes do CSS (Jeito Roots)
Utilizamos a propriedade .classList.add("CLASSE"). Desta forma, ao clicar será adicionado uma classe e o comportamento irá mudar.

```
const $heart = document.querySelector(".-heart");

$heart.addEventListener("click",handleClick);

    function handleClick(){
    $heart.classList.add("-active");
    }
```
No .-heart foi adicinado ao final -active (adicionado no CSS) alterando a cor de fundo.

### Modificando Classes do CSS (Jeito Top)
```
$heart.classList.toggle("-active")
```
a propriedade .toggle verifica a classe entre() existe. Caso não ele adiciona, caso sim ele remove.

# classList
Add - Adiciona classe CSS
Remove - Remove classe CSS
Toggle - Verifica a classe entre() existe. Caso não ele adiciona, caso sim ele remove.