const $heart = document.querySelector(".-heart");
const $stars = document.querySelectorAll(".star");
const positionLast = $stars.length - 1;

$heart.addEventListener("click", handleClick);

$stars[0].addEventListener("click",handleClick);

$stars[positionLast].addEventListener("click",lastStar);

console.log(positionLast);

function handleClick(){
    console.log(this);
    this.classList.toggle("-active")
}


function lastStar(){
    $stars.forEach(function($star){
        $star.classList.add("-active");
    })
}